/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

/**
 * Gravity.
 */
public class Gravity {
    /**
     * * The constant AXIS_SPECIFIED
     */
    public static final int AXIS_SPECIFIED = 0x0001;

    /**
     * * The constant AXIS_PULL_BEFORE
     */
    public static final int AXIS_PULL_BEFORE = 0x0002;

    /**
     * * The constant AXIS_PULL_AFTER
     */
    public static final int AXIS_PULL_AFTER = 0x0004;

    /**
     * * The constant AXIS_CLIP
     */
    public static final int AXIS_CLIP = 0x0008;

    /**
     * * The constant AXIS_X_SHIFT
     */
    public static final int AXIS_X_SHIFT = 0;

    /**
     * * The constant AXIS_Y_SHIFT
     */
    public static final int AXIS_Y_SHIFT = 4;

    /**
     * * The constant RELATIVE_LAYOUT_DIRECTION
     */
    public static final int RELATIVE_LAYOUT_DIRECTION = 0x00800000;

    /**
     * * The constant TOP
     */
    public static final int TOP = (AXIS_PULL_BEFORE | AXIS_SPECIFIED) << AXIS_Y_SHIFT;

    /**
     * * The constant BOTTOM
     */
    public static final int BOTTOM = (AXIS_PULL_AFTER | AXIS_SPECIFIED) << AXIS_Y_SHIFT;

    /**
     * * The constant LEFT
     */
    public static final int LEFT = (AXIS_PULL_BEFORE | AXIS_SPECIFIED) << AXIS_X_SHIFT;

    /**
     * * The constant RIGHT
     */
    public static final int RIGHT = (AXIS_PULL_AFTER | AXIS_SPECIFIED) << AXIS_X_SHIFT;

    /**
     * * The constant START
     */
    public static final int START = RELATIVE_LAYOUT_DIRECTION | LEFT;

    /**
     * * The constant END
     */
    public static final int END = RELATIVE_LAYOUT_DIRECTION | RIGHT;
}
