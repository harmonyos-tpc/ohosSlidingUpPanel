/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

/**
 * AnimationProcessor.
 */
class AnimationProcessor {
    private SlidingUpBuilder mBuilder;
    private ValueAnimator mValueAnimator;
    private float mSlideAnimationTo;

    AnimationProcessor(SlidingUpBuilder builder, AnimatorValue.ValueUpdateListener updateListener,
                    Animator.StateChangedListener listener) {
        mBuilder = builder;
        createAnimation(updateListener, listener);
    }

    void endAnimation() {
        if (mValueAnimator != null && mValueAnimator.isRunning()) {
            mValueAnimator.end();
        }
    }

    boolean isAnimationRunning() {
        return mValueAnimator != null && mValueAnimator.isRunning();
    }

    void setValuesAndStart(float from, float to) {
        mSlideAnimationTo = to;
        mValueAnimator.setFloatValues(from, to);
        mValueAnimator.start();
    }

    private void createAnimation(AnimatorValue.ValueUpdateListener updateListener,
                                Animator.StateChangedListener listener) {
        mValueAnimator = ValueAnimator.ofFloat();
        mValueAnimator.setDuration(Constants.THIRTY_NUM_CONST);
        mValueAnimator.setInterpolator(mBuilder.isInterpolator);
        mValueAnimator.setValueUpdateListener(updateListener);
        mValueAnimator.setStateChangedListener(listener);
    }
}
