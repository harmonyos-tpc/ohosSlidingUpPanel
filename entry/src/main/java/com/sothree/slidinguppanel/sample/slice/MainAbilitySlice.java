/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel.sample.slice;

import com.sothree.slidinguppanel.Constants;
import com.sothree.slidinguppanel.Gravity;
import com.sothree.slidinguppanel.SlidingUpBuilder;
import com.sothree.slidinguppanel.SlidingUpPanel;
import com.sothree.slidinguppanel.sample.ResourceTable;
import com.sothree.slidinguppanel.sample.adapter.SampleListItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ListDialog;

import java.util.Arrays;
import java.util.List;

/**
 * MainAbilitySlice.
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private SlidingUpPanel slidingUpPanel;
    private Component dim;
    private Component sliderView;
    private Component mainLayout;
    private ListContainer listContainer;
    private SampleListItemProvider sampleListItemProvider;
    private Text mainContentText;
    private Text name;
    private Button follow;
    private String[] item = new String[]{"Hide Panel", "Show Panel"};
    private boolean flag = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        sliderView = findComponentById(ResourceTable.Id_slideView);
        dim = findComponentById(ResourceTable.Id_dim);
        if (findComponentById(ResourceTable.Id_listcontainer) instanceof ListContainer) {
            listContainer = (ListContainer) findComponentById(ResourceTable.Id_listcontainer);
        }
        mainLayout = findComponentById(ResourceTable.Id_mainLayout);
        if (findComponentById(ResourceTable.Id_main_content_text) instanceof Text) {
            mainContentText = (Text) findComponentById(ResourceTable.Id_main_content_text);
        }
        if (findComponentById(ResourceTable.Id_name) instanceof Text) {
            name = (Text) findComponentById(ResourceTable.Id_name);
        }
        if (findComponentById(ResourceTable.Id_follow) instanceof Button) {
            follow = (Button) findComponentById(ResourceTable.Id_follow);
        }
        initView();
        slidingUpPanel = new SlidingUpBuilder(sliderView)
                .withListeners(new SlidingUpPanel.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / Constants.HUNDERED_NUM_CONST));
                        if (percent > Constants.EIGHTY_NUM_CONST) {
                            flag = false;
                        } else {
                            flag = true;
                        }
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlidingUpPanel.State.HIDDEN)
                .withSlideFromOtherView(findComponentById(ResourceTable.Id_rootView))
                .build();
        name.setText(ResourceTable.String_name_description);
        follow.setText(ResourceTable.String_follow);
        name.setClickedListener(this);
        follow.setClickedListener(this);
        mainLayout.setClickedListener(this);
        initChangeView();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initView() {
        List<String> listval = Arrays.asList(
                "This",
                "Is",
                "An",
                "Example",
                "ListView",
                "That",
                "You",
                "youcan",
                "Scroll",
                ".",
                "It is",
                "Show s",
                "How will",
                "Any way",
                "Scrollable",
                "View all",
                "Mis sss",
                "Be careful",
                "Included",
                "Asc",
                "You v",
                "Can we",
                "Scroll top",
                ".",
                "It is",
                "Shows",
                "How nnn",
                "Any",
                "Scrollable",
                "View",
                "Can do",
                "A",
                "Child m",
                "Of",
                "SlidingUpPanelLayout"
        );

        sampleListItemProvider = new SampleListItemProvider(this, listval);
        listContainer.setItemProvider(sampleListItemProvider);
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_name || component.getId() == ResourceTable.Id_follow
                || component.getId() == ResourceTable.Id_mainLayout) {
            if (!flag) {
                slidingUpPanel.show();
                flag = true;
            } else {
                slidingUpPanel.notifySlidingHide();
                flag = false;
            }
        }
    }

    private void initChangeView() {
        findComponentById(ResourceTable.Id_change_view).setClickedListener(new Component.ClickedListener() {
            void gotoMenuDetails(int position) {
                switch (position) {
                    case 0:
                        slidingUpPanel.hide();
                        break;
                    case 1:
                        slidingUpPanel.show();
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            }

            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(MainAbilitySlice.this);
                listDialog.setSingleSelectItems(item, Constants.INVALID);
                listDialog.setOnSingleSelectListener((iDialog, position) -> {
                    gotoMenuDetails(position);
                    listDialog.hide();
                });
                listDialog.setButton(Constants.ZERO_NUM_CONST, "Okay",
                        (iDialog, i) -> listDialog.destroy());
                listDialog.show();
            }
        });
    }
}